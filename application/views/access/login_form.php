<div style="height: 100vh">
    <div class="flex-center flex-column">

        <h3 class="mb-5">Controle Financeiro Pessoal</h3>
    
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Entrar</strong>
            </h5>
            <div class="card-body px-lg-5 pt-0">
                <form class="text-center" style="color: #757575;" method="POST">
                    <div class="md-form">
                        <input type="email" id="email" class="form-control" name="email">
                        <label for="email">E-mail</label>
                    </div>
                    <div class="md-form">
                        <input type="password" id="senha" class="form-control" name="senha">
                        <label for="senha">Senha</label>
                    </div>
                    <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Enviar</button>
                    <p class="red-text"><?= $error ? 'Dados de acesso incorretos.' : '' ?></p>
                </form>

                
            </div>
        </div>       
    </div>
</div>
