<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto border mt-5 pt-3 pb-3">
            <form method="POST" id="contas-form">
                <input type="text" name="parceiro" class="form-control" placeholder='Devedor / Credor'><br/>
                <input type="text" name="descricao" class="form-control" placeholder='Descrição'><br/>
                <input type="number" name="valor" class="form-control" placeholder='Valor'><br/><br/>
                <input type="hidden" name="tipo" value="<?= $tipo ?>">

                <div class="text-center teext-md-left">
                    <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();">Send</a>
                </div>

            </form>
        </div>
    </div>
</div>