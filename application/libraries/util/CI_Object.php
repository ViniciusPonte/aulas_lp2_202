<?php

class CI_Object{
    /**
     * @param string $key
     */
    public function __get($key){
        return get_instance()->$key;
    }
}